import { TokenInterceptorService } from './interceptors/token-interceptor.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { SignInUpComponent } from './components/sign-in-up/sign-in-up.component';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { InputsComponentComponent } from './components/navigation/inputs-component/inputs-component.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavigationComponent } from './components/navigation/navigation.component';
import { ListInputsComponent } from './components/navigation/list-inputs/list-inputs.component';
@NgModule({
  declarations: [
    AppComponent,
    SignInUpComponent,
    InputsComponentComponent,
    NavigationComponent,
    ListInputsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass : TokenInterceptorService,
    multi : true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
