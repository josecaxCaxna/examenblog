import { TestBed } from '@angular/core/testing';

import { DataConnectApiRestService } from './data-connect-api-rest.service';

describe('DataConnectApiRestService', () => {
  let service: DataConnectApiRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataConnectApiRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
