import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class DataConnectApiRestService {
  User;
  token;
  constructor(private http: HttpClient , private router : Router ) { }
  URL = 'http://localhost/examen/backend/public/api/'

  /**
   * @description Funcion para iniciar sesion
   * @param data
   * @returns subscriber
   */
  login(data){
    return this.http.post(`${this.URL}login`,data);
  }

  /**
   *@description Funcion para cerrar sesión destruyendo el Token en localStorage
   * @returns void
   */
  signout(){
    return this.http.post(`${this.URL}logout`,'').pipe(
      map(
        res=>{
          if (res['logout']) {
            localStorage.clear();
            this.router.navigate(['login']);
          }
        }
      )
    );
  }

  /**
   * @description Funcion para obtener el usuario logueado
   * @returns User
   */
  getUserLoggued(a){
    return this.http.get(`${this.URL}me`).subscribe(
      res=>{
        this.User = res;
        localStorage.setItem('currentUser', JSON.stringify(this.User));
      },
      err=>{
        console.error(err);
      }
    );
  }

  /**
   * @description Funcion que registra las entradas del blog
   * @param data
   * @returns subscriber
   */
  postBlog(data){
    return this.http.post(`${this.URL}createInput`,data);
  }

  /**
   * @description Función para enviar las imagenes adjuntadas en cada entrada
   * @param data
   * @returns Json Confimation
   */
  postImages(data){
    return this.http.post(`${this.URL}createInputImages`,data);
  }

  getAllInputs(){
    return this.http.get(`${this.URL}listInputs`);
  }
}
