import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor() { }

  /**
   * @description Funcion que setea el Token por cada peticion que se realiza
   * @param req
   * @param next
   * @returns cloneReq --> Peticion clonada con los headers
   */
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let currentToken = JSON.parse(localStorage.getItem('currentToken'));
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${currentToken}`
    });
    const cloneReq = req.clone( { headers : headers } );
    return next.handle( cloneReq );
  }
}
