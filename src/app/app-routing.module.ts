import { NavigationComponent } from './components/navigation/navigation.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignInUpComponent } from './components/sign-in-up/sign-in-up.component';
import { InputsComponentComponent } from './components/navigation/inputs-component/inputs-component.component';
import { ListInputsComponent } from './components/navigation/list-inputs/list-inputs.component';
const routes: Routes = [
  { path: 'login', component: SignInUpComponent },
  { path: 'navigation' , component: NavigationComponent,
    children:[
      { path: 'inputs', component: InputsComponentComponent },
      { path: 'listInputs', component: ListInputsComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
