import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataConnectApiRestService } from "../../services/data-connect-api-rest.service";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-sign-in-up',
  templateUrl: './sign-in-up.component.html',
  styleUrls: ['./sign-in-up.component.css']
})
export class SignInUpComponent implements OnInit {
  form = new FormGroup({
    email: new FormControl(null,[Validators.required, Validators.email]),
    password: new FormControl(null,[Validators.required])
  });
  token;
  constructor(private service: DataConnectApiRestService, private router : Router) { }

  ngOnInit(): void {
  }

  onSubmit() {
    if (this.form.valid) {

      this.service.login(this.form.value).subscribe(
        res=>{
          if (res['success']) {
            this.form.reset();
            this.token = res['access_token'];
            this.router.navigate(['navigation/inputs']);
            localStorage.setItem('currentToken', JSON.stringify(this.token));
            this.service.getUserLoggued(this.token);
          }else{
            if (!res['success'] && res['message']=== 'Invalid Email or Password') {
              Swal.fire({
                title: 'Error!',
                text: 'Las credenciales son incorrectas',
                icon: 'error',
                confirmButtonText: 'Ok'
              })
            }else{
              console.error("incorrect",res);
              Swal.fire({
                title: 'Error!',
                text: `${res['message']} - ${res['success']}`,
                icon: 'error',
                confirmButtonText: 'Ok'
              })
            }
          }
        },
        err=>{
          console.error(err);
        }
      );
    }
  }

  reset() {
    this.form.reset();
  }
}
