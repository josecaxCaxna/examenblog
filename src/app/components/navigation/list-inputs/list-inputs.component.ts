import { Component, OnInit } from '@angular/core';
import { DataConnectApiRestService } from 'src/app/services/data-connect-api-rest.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-inputs',
  templateUrl: './list-inputs.component.html',
  styleUrls: ['./list-inputs.component.css']
})
export class ListInputsComponent implements OnInit {
  inputs :any = [];
  dataLength:boolean;
  constructor(private service: DataConnectApiRestService) { }

  ngOnInit(): void {
    this.getAllInputs();
  }

  getAllInputs(){
    this.service.getAllInputs().subscribe(
      res=>{
        if (res['success']) {
          this.inputs = res['message'];
          console.log(res);
          console.log(this.inputs.length);
          if (this.inputs.length != 0) {
            this.dataLength = true;
          }else{
            this.dataLength = false;
          }
        }
      },
      err=>{
        console.error(err);
        Swal.fire({
          title: 'Error!',
          text: 'Ha ocurrido un error al obtener los datos',
          icon: 'error',
          confirmButtonText: 'Ok'
        });
      }
    );
  }
}
