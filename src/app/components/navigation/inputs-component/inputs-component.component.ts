import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataConnectApiRestService } from 'src/app/services/data-connect-api-rest.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-inputs-component',
  templateUrl: './inputs-component.component.html',
  styleUrls: ['./inputs-component.component.css']
})
export class InputsComponentComponent implements OnInit {
  form = new FormGroup({
    tittle: new FormControl(null,[Validators.required]),
    data: new FormControl(null,[Validators.required]),
    slug: new FormControl(null,[Validators.required]),
    file:new FormControl(null,[Validators.required])
  });
  images = [];
  formImages = [];
  constructor( private service : DataConnectApiRestService ) { }

  ngOnInit(): void {
  }

  onSubmit(f) {
    const formData = new FormData();
    this.formImages.forEach((file) => {
      formData.append('file[]', file);
    });
    if (f.status === "VALID") {
      this.service.postBlog(f.value).subscribe(
        res=>{
          if (res['success']) {
            // this.swalMessage('Correcto!','El registro ha sido creado correctamente','success');
            this.form.reset();
            this.images = [];
            this.formImages = [];
            this.service.postImages(formData).subscribe(
              res=>{
                if (res['success']) {
                  this.swalMessage('Correcto!','El registro ha sido creado correctamente','success');
                }
              },
              err=>{
                this.swalMessage('Error!','Ocurrio un error al cargar las imágenes','error');
                console.error(err);
              }
            )
          }
        },
        err=>{
          this.swalMessage('Error!','Ocurrio un error al cargar los datos del formulario','error');
          console.error(err);
        }
      );
    }else{
      this.swalMessage('Error!','Debe llenar todos los campos del formulario','error');
    }
  }

  onFileChange(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        if ( event.target.files[i].type === "image/png" || event.target.files[i].type === "image/gif" || event.target.files[i].type === "image/jpeg" || event.target.files[i].type === "image/jpg") {
          var reader = new FileReader();
          this.formImages.push(<File>event.target.files[i]);
          reader.onload = (event:any) => {
            this.images.push(event.target.result);
            this.form.patchValue({ fileSource: this.images });
          }
          reader.readAsDataURL(event.target.files[i]);
        }else{
          this.images = [];
          this.formImages = [];
          this.form.controls.file.setValue(null,{ emitModelToViewChange: true });
          this.swalMessage('Error!','Debe seleccionar solo imágenes','error');
        }
      }
    }
  }

  resetImages(){
    this.images = [];
    this.formImages = [];
    this.form.controls.file.setValue(null,{
      emitModelToViewChange: true
    });
  }

  swalMessage(title,text,icon){
    Swal.fire({
      title: title,
      text: text,
      icon: icon,
      confirmButtonText: 'Ok'
    });
  }
}
