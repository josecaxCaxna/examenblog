import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputsComponentComponent } from './inputs-component.component';

describe('InputsComponentComponent', () => {
  let component: InputsComponentComponent;
  let fixture: ComponentFixture<InputsComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputsComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
