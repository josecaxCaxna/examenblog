import { DataConnectApiRestService } from 'src/app/services/data-connect-api-rest.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  constructor(private service: DataConnectApiRestService) { }

  ngOnInit(): void {
  }

  logout(){
    this.service.signout()
    .subscribe(res=>{
      // console.log(res);
    },
    err=>{
      console.error(err);
    });
  }
}
